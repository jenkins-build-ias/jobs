multibranchPipelineJob('ds-project-template') {
    branchSources {
        git {
            id('ds-project-template')
            remote('git@bitbucket.org:dsintegration/ds-project-template.git')
        }
    }
    triggers {
        periodic(1)
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(0)
            daysToKeep(3)
        }
    }
}


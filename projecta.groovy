multibranchPipelineJob('project-a') {
    branchSources {
        git {
            id('1')
            remote('https://gitlab.com/jenkins-build-ias/project-a.git')
        }
    }
    triggers {
        periodic(1)
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(0)
            daysToKeep(3)
        }
    }
}


multibranchPipelineJob('project-b') {
    branchSources {
        git {
            id('2')
            remote('https://gitlab.com/jenkins-build-ias/project-b.git')
        }
    }
    triggers {
        periodic(1)
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(0)
            daysToKeep(3)
        }
    }
}

